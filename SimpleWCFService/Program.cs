﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
 
namespace HttpsSelfhost
{
    class Program
    {
        static Type m_ServiceType = typeof(HelloWorldService);
        static string m_CertAddress = "0.0.0.0";
        static string m_CertDescriptor = "localhost";
        static string m_ServiceAddress = "localhost";
        static string m_ServiceEndpoint = "HelloWorldService";
        static int m_PortNumber = 7654;
        static string m_ServiceUrl = "https://localhost:4321/HelloWorldService";

        static void Main(string[] args)
        {
            bool bMakeCerts = false;

            for(int iArg=0;iArg<args.Length;++iArg) {
                System.Diagnostics.Debug.WriteLine(args[iArg]);
                string arg = args[iArg].ToLower();
                if(arg == "-mcert")
                {
                    bMakeCerts = true;
                }
                if (arg == "-p" && ++iArg<arg.Length)
                {
                    int.TryParse(args[iArg],out m_PortNumber);
                }
            }
            m_ServiceUrl = "https://" + m_ServiceAddress + ":" + m_PortNumber.ToString() + "/" + m_ServiceEndpoint;
            if (bMakeCerts)
            {
                createAndRegisterCerts();
            }
            else {
                //MAKE PORT IS BOUND TO AN SSL PORT USING THE CERTGEN UTIL
                hostService(); 
            }
        }

        static void hostService()
        {
            Task.Run(() =>
            {
                try
                {
                    //ServiceHost host = createService();
                    ServiceHost host = createConfigBasedService();
                    //addServiceBehaviors(host);


                    int endpointCount = host.Description.Endpoints.Count;
                    int behaviorsCount = host.Description.Behaviors.Count;

                    host.Open();

                    Console.WriteLine("Host is {0}.  Press enter to close.", host.State);
                    Console.ReadLine();
                    host.Close();
                }
                catch(Exception x)
                {
                    string sErr = x.Message;
                }
            }).Wait();
        }
        static ServiceHost createConfigBasedService(bool bAddBaseAddress = false)
        {
            ServiceHost host = null;
            if (bAddBaseAddress)
            {
                host = new ServiceHost(m_ServiceType, new Uri[] {
                    new Uri(m_ServiceUrl)
                });
            }
            else
            {
                host = new ServiceHost(m_ServiceType);
            }
            return host;
        }
        static ServiceHost createService()
        {
            string address = m_ServiceUrl;
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.Transport;

            ServiceHost host = new ServiceHost(typeof(HelloWorldService), new Uri[] {
                    new Uri(address)
                });
            ServiceEndpoint SEP = host.AddServiceEndpoint(typeof(IHelloWorld), binding, address);
            return host;
        }
        static void addServiceBehaviors(ServiceHost host)
        {
            ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
            smb.HttpsGetEnabled = true;
            //smb.HttpsGetUrl = host.Description.Endpoints[0].Address.Uri;
            smb.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
            host.Description.Behaviors.Add(smb);

            //host.AddServiceEndpoint(
            //  ServiceMetadataBehavior.MexContractName,
            //  MetadataExchangeBindings.CreateMexHttpsBinding(),
            //  "mex"
            //);
        }
        static void createAndRegisterCerts()
        {
            try
            {
                string addressAndPort = m_CertDescriptor + ":" + m_PortNumber.ToString();
                string issuerName = "CN=Z Dev VI";
                string certName = "CN=" + m_CertDescriptor;

                var applicationId = ((GuidAttribute)typeof(Program).Assembly.GetCustomAttributes(typeof(GuidAttribute), true)[0]).Value;

                //var sslCert = executeCommand("netsh http show sslcert " + m_CertAddress + ":" + m_PortNumber.ToString());
                //Console.WriteLine();
                //if (sslCert.IndexOf(applicationId, StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    Console.WriteLine("Removing existing instance..");
                    Console.WriteLine(executeCommand("netsh http delete sslcert ipport=" + m_CertAddress + ":" + m_PortNumber.ToString()));
                }

                CertGen.Gen keyGen = new CertGen.Gen();
                var caPrivKey = keyGen.GenerateCACertificate(issuerName);
                var cert = keyGen.GenerateSelfSignedCertificate(certName, issuerName, caPrivKey);
                keyGen.AddCertToStore(cert, StoreName.My, StoreLocation.LocalMachine);

                string sCmd = @"netsh http add sslcert ipport=" + m_CertAddress + ":" + m_PortNumber.ToString();
                sCmd += @" certhash=" + cert.Thumbprint;
                sCmd += @"  appid={" + applicationId + "}";
                Console.WriteLine(executeCommand(sCmd));

            }
            catch (Exception x)
            {
                string sErr = x.Message;
            }
        }
        static string executeCommand(string action)
        {
            StringBuilder stringBuilder = new StringBuilder();
            using (Process process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    WindowStyle = ProcessWindowStyle.Normal,
                    FileName = "cmd.exe",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    Arguments = "/c " + action
                }
            })
            {
                Console.WriteLine("Executing Command:");
                Console.WriteLine(action);
                process.Start();

                while (!process.StandardOutput.EndOfStream)
                {
                    stringBuilder.AppendLine(process.StandardOutput.ReadLine());
                }
                int exitCode = process.ExitCode;
                process.Close();

            }

            return stringBuilder.ToString();
        }
    }
 
    [ServiceContract]
    public interface IHelloWorld
    {
        [OperationContract]
        void HelloWorld();
    }
 
    public class HelloWorldService : IHelloWorld
    {
        public void HelloWorld()
        {
            Console.WriteLine("Hello World!");
        }
    }
}